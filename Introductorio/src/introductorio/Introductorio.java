/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package introductorio;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class Introductorio {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        byte n3 = 127;
        short n2 = 32658;
        int n1 = 12;
        long n4 = 12312312;

        float n5 = (float) 123.23;
        double n6 = 123.23;

        char caracter = '\u00b5';
        System.out.println(caracter);

        System.out.println("A\tB\tC\n1\t2\t3");
        System.out.println("\"HOLA\"");
        System.out.println("El caracter de escape \\n cambia de línea");
        System.out.println("El unicode \\u0040 vale \u0040");
        System.out.println("El unicode \\u00D1 vale \u00D1");

        int num1;  // Declarar
        num1 = 12; // Inicializar(el primer valor de la variable)
        num1 = 23; // Asignación

        int num2 = 13; // Declaración e inicialización en 1 paso

        final int CAN_MIN = 10;

        boolean isReal = true;
        byte b = 122;
        short s = -29000;
        int i = 100000;
        long l = 999999999999L;
        float f1 = 234.99F;
        double d;
        char cvalue = '4';
        System.out.println(cvalue);
        final double PI = 3.1415926;

        //Pedir con una ventana 
//        String nombre = JOptionPane.showInputDialog("Dite su nombre: ");
//        JOptionPane.showMessageDialog(null, "Usted se llama " + nombre, "Nombre...",
//                JOptionPane.PLAIN_MESSAGE);
//
//        int res = JOptionPane.showConfirmDialog(null, "¿Está seguro que desa salir?",
//                "Salir", JOptionPane.YES_NO_OPTION);
//        System.out.println(res);
//
//        if (res == JOptionPane.NO_OPTION) {
//            //Pedir datos en consola
//            Scanner sc = new Scanner(System.in);
//            System.out.print("Digite su nombre: ");
//            nombre = sc.nextLine();
//            System.out.print("Edad: ");
//            int edad = sc.nextInt();
//        }
//        System.out.println("Gracias por usar nuestra aplicación");
        // - Nombre
        // - Apellido
        // - Teléfono 
        // - Genero
        // - Trabaja SI/NO
        // - SI: 
        //      - Lugar de trabajo
        //      - Salario mensual
        // 
        // Mostrar un resumen de los datos
        String nom = JOptionPane.showInputDialog(null, "Nombre", "Entrevista", JOptionPane.QUESTION_MESSAGE);
        String ape = JOptionPane.showInputDialog(null, "Apellidos", "Entrevista", JOptionPane.QUESTION_MESSAGE);
        String tel = JOptionPane.showInputDialog(null, "Teléfono", "Entrevista", JOptionPane.QUESTION_MESSAGE);

        String[] generos = {"Femenino", "Masculino"};

        Object gen = JOptionPane.showInputDialog(null, "Género", "Entrevista",
                JOptionPane.QUESTION_MESSAGE, null, generos, generos[0]);

        String txt = "Nombre: %s %s\n"
                + "Teléfono: %s\n"
                + "Género: %s";

        String temp = String.format("%s %d %.2f", "Resultado: ", 2, 2.3);
        System.out.println(temp);

        String res = String.format(txt, nom, ape, tel, (String) gen);

        int tra = JOptionPane.showConfirmDialog(null, "¿Trabaja?",
                "Entrevista", JOptionPane.YES_NO_OPTION);

        if (tra == JOptionPane.YES_OPTION) {
            String lug = JOptionPane.showInputDialog(null, "Lugar de Trabajo", "Entrevista", JOptionPane.QUESTION_MESSAGE);
            String sal = JOptionPane.showInputDialog(null, "Salario Mensual", "Entrevista", JOptionPane.QUESTION_MESSAGE);
            res += "\nLugar Trabajo: " + lug + "\nSalario Mensual: " + sal;
        }
        JOptionPane.showMessageDialog(null, res);
        
//        String edadTxt = JOptionPane.showInputDialog("Edad");
//        int edad = Integer.parseInt(edadTxt);
//        double x = Double.parseDouble(edadTxt);        
//        int edad2 = Integer.parseInt(JOptionPane.showInputDialog("Edad"));
        

//        Scanner sc  = new Scanner(System.in);
//        System.out.print("Edad: ");
//        int x = sc.nextInt();
//        System.out.println(x);
    }

}
