/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package introductorio;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class TipoCambio {

    public static void main(String[] args) {
        //     Tipo Cambio - UTN v0.1
        // - ¿Valor de $1 en colones ? 582
        // - ¿Colones a convertir? 100000 (₡/TC)
        // - ₡100000 equivalen a $171.82 
        //
        // - ¿Dólares a convetir? 150 ($*TC)
        // - $150 equivalen a ₡87300

        float tc = Float.parseFloat(JOptionPane.showInputDialog("¿Valor de $1 en colones ?"));
        float cc = Integer.parseInt(JOptionPane.showInputDialog("¿Colones a convertir?"));
        String res = "%s%.0f equivalen a %s%.2f";
        JOptionPane.showMessageDialog(null, String.format(res, "₡", cc, "$", cc / tc));
        float dc = Integer.parseInt(JOptionPane.showInputDialog("¿Dólares a convetir?"));
        JOptionPane.showMessageDialog(null, String.format(res, "$", dc, "₡", dc * tc));

    }
}
