/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import util.Util;

/**
 *
 * @author Allan Murillo
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();
        log.setJugadorUno(Util.leerTexto("Jugador Uno"));
        log.setJugadorDos(Util.leerTexto("Jugador Dos"));
        boolean termino = false;

        do {
            String txt = "Jugador Actual: %s\n"
                    + "\n%s\n";
            int f = Util.leerInt(String.format(txt, log.getJugadorActual(), log.imprimir()) + "\nFila:");
            int c = Util.leerInt(String.format(txt, log.getJugadorActual(), log.imprimir()) + "Fila: " + f + "\nColumna:");
            log.marcar(f, c);
            if (log.gano()) {
                Util.mostrar(log.imprimir() + "\nGanador: " + log.getGanador());
                termino = true;
            } else if (log.empate()) {
                Util.mostrar(log.imprimir() + "\nJuego empatado");
                termino = true;
            }
            if (termino) {
                if (Util.confirmar("¿Desea jugar nuevamente?")) {
                    log.reiniciar();
                    termino = false;
                } else {
                    break;
                }
            }
        } while (true);

    }

}
