/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    private final char[][] tablero;

    private String jugadorUno;
    private String jugadorDos;
    private boolean turno;

    private final char MARCA_J1 = 'X';
    private final char MARCA_J2 = 'O';

    public Logica() {
        tablero = new char[3][3];
        limpiar();
        rifar();
    }

    public void reiniciar() {
        limpiar();
        rifar();
    }

    /**
     * Rifa el jugador inicial a partir de un número aleatorio
     */
    private void rifar() {
        int ale = (int) (Math.random() * 10) + 1;
        turno = ale <= 5;
    }

    /**
     * Reemplazar los caracteres de la matriz(tablero) por un guión bajo(_)
     */
    private void limpiar() {
        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {
                tablero[f][c] = '_';
            }
        }
    }

    /**
     * Genera un texto con los datos de la matriz con un formato de filas y
     * columnas
     *
     * @return String con la matriz
     */
    public String imprimir() {
        String txt = String.format("%4d%4d%4d\n", 1, 2, 3);
        int numFil = 1;
        for (char[] fila : tablero) {
            txt += numFil + " ";
            for (char columna : fila) {
                txt += columna + "  " + (columna == '_' ? " " : "");
            }
            numFil++;
            txt += "\n";
        }
        return txt;
    }

    /**
     * Marca la casilla correspondiente dependiendo del turno del jugador
     *
     * @param fila int número de fila a marcar (de 1 a 3)
     * @param columna int número de columna a marcar (de 1 a 3)
     */
    public void marcar(int fila, int columna) {
        if (fila > 0 && fila < 4 && columna > 0 && columna < 4) { //marca este dentro del tablero
            if (tablero[fila - 1][columna - 1] == '_') { //La casilla este vacía
                tablero[fila - 1][columna - 1] = turno ? MARCA_J1 : MARCA_J2; //Marco dependiendo del turno
                turno = !turno;//invertimos el turno para el cambio correspondiente
            }
        }
    }

    public boolean gano() {
        //3 Filas
        for (int f = 0; f < 3; f++) {
            if (tablero[f][0] != '_' && tablero[f][0] == tablero[f][1] && tablero[f][0] == tablero[f][2]) {
                return true;
            }
        }
        //3 Columnas
        for (int c = 0; c < 3; c++) {
            if (tablero[0][c] != '_' && tablero[0][c] == tablero[1][c] && tablero[0][c] == tablero[2][c]) {
                return true;
            }
        }
        //Diagonales
        if (tablero[0][0] != '_' && tablero[0][0] == tablero[1][1] && tablero[0][0] == tablero[2][2]) {
            return true;
        }
        if (tablero[0][2] != '_' && tablero[0][2] == tablero[1][1] && tablero[0][2] == tablero[2][0]) {
            return true;
        }
        return false;
    }

    public boolean empate() {
        for (char[] fila : tablero) {
            for (char col : fila) {
                if (col == '_') {
                    return false;
                }
            }
        }
        return true;
    }

    public void setJugadorUno(String jugadorUno) {
        this.jugadorUno = jugadorUno;
    }

    public void setJugadorDos(String jugadorDos) {
        this.jugadorDos = jugadorDos;
    }

    public String getJugadorActual() {
        return turno ? jugadorUno + "(" + MARCA_J1 + ")" : jugadorDos + "(" + MARCA_J2 + ")";
    }

    public String getGanador() {
        return turno ? jugadorDos : jugadorUno;
    }
}
