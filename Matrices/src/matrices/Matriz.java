/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

/**
 *
 * @author Allan Murillo
 */
public class Matriz {

    private final int[][] numeros;

    public Matriz() {
        numeros = new int[5][5];
    }

    public Matriz(int filas, int columnas) {
        numeros = new int[filas][columnas];
    }

    public String imprimir() {
        String res = "";

        for (int[] fila : numeros) {
            for (int espacio : fila) {
                res += espacio + "  ";
            }
            res += "\n";
        }

        return res;
    }

    public void llenarMatriz() {
        for (int f = 0; f < numeros.length; f++) {
            for (int c = 0; c < numeros[f].length; c++) {
                numeros[f][c] = (int) (Math.random() * 8) + 1;
            }
        }
    }

    public void convertirPares() {
        for (int f = 0; f < numeros.length; f++) {
            for (int c = 0; c < numeros[f].length; c++) {
                if (numeros[f][c] % 2 != 0) {
                    numeros[f][c]++;
                }
            }
        }
    }

    public void convertirImpares() {
        for (int f = 0; f < numeros.length; f++) {
            for (int c = 0; c < numeros[f].length; c++) {
                if (numeros[f][c] % 2 == 0) {
                    numeros[f][c]++;
                }
            }
        }
    }

    public double promedio() {
        double sum = 0;
        int can = 0;

        for (int[] row : numeros) {
            for (int esp : row) {
                sum += esp;
                can++;;
            }
        }

        return sum / can;
    }

    public String promedioFila() {
        String txt = "";
        for (int[] fila : numeros) {
            double sum = 0;
            int col = 0;
            for (int esp : fila) {
                txt += esp + " ";
                sum += esp;
                col++;
            }
            txt += String.format("= %.1f\n", sum / col);
        }
        return txt;
    }

    public String promedioColumna() {
        String txt = "";
        for (int c = 0; c < numeros[0].length; c++) {
            double sum = 0;
            int col = 0;
            for (int f = 0; f < numeros.length; f++) {
                int esp = numeros[f][c];
                txt += esp + " ";
                sum += esp;
                col++;
            }
            txt += String.format("= %.1f\n", sum / col);
        }
        return txt;
    }
}