/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrices;

import util.Util;

/**
 *
 * @author Allan Murillo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String menu = "1. Convertir a pares\n"
                + "2. Convertir a impares\n"
                + "3. Promedio\n"
                + "4. Promedio por fila\n"
                + "5. Promedio por columna\n"
                + "6. Salir";

        Matriz m = new Matriz();
        m.llenarMatriz();
        Util.mostrar(m.imprimir());
        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    String res = "Antes--> \n" + m.imprimir() + "\n\n";
                    m.convertirPares();
                    res += "Después: -->\n" + m.imprimir();
                    Util.mostrar(res);
                    break;
                case 2:
                    res = "Antes--> \n" + m.imprimir() + "\n\n";
                    m.convertirImpares();
                    res += "Después: -->\n" + m.imprimir();
                    Util.mostrar(res);
                    break;
                case 3:
                    res = "Antes--> \n" + m.imprimir() + "\n\n";
                    res += "Promedio: %.1f";
                    Util.mostrar(String.format(res, m.promedio()));
                    break;
                case 4:
                    res = "Promedio por Fila: \n\n" + m.promedioFila();
                    Util.mostrar(res);
                    break;
                case 5:
                    res = "Antes--> \n" + m.imprimir() + "\n\n";
                    res += "Promedio por Columna: \n\n" + m.promedioColumna();
                    Util.mostrar(res);
                    break;
                case 6:
                    break APP;
            }
        } while (true);
    }
}
