/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciouno;

import util.Util;

/**
 *
 * @author Allan Murillo
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();
        String menu = "1. Ingresar Vehículo\n"
                + "2. Buscar\n"
                + "3. Listar\n"
                + "4. Salir";

        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    Vehiculo v = new Vehiculo();
                    v.setPlaca(Util.leerInt("Placa"));
                    v.setCliente(Util.leerTexto("Cliente"));
                    v.setKm(Util.leerInt("Kilometros"));
                    log.agregar(v);
                    break;
                case 2:
                    Util.mostrar(log.buscar());
                    break;
                case 3:
                    Util.mostrar(log.listar());
                    break;
                case 4:
                    break APP;
            }
        } while (true);

    }
}
