/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciouno;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    private Vehiculo[] alquileres;

    public Logica() {
        alquileres = new Vehiculo[20];
    }

    public void agregar(Vehiculo v) {
        for (int i = 0; i < alquileres.length; i++) {
            if (alquileres[i] == null) {
                alquileres[i] = v;
                break;
            }
        }
    }

    public String listar() {
        String lista = "";
        for (Vehiculo v : alquileres) {
            if (v != null) {
                lista += String.format("%d | %5d | %s  \n", v.getPlaca(), v.getKm(), v.getCliente());
            }
        }
        return lista;
    }

    public String buscar() {
        String lista = "";
        for (Vehiculo v : alquileres) {
            if (v != null && v.getKm() > 20 && v.getKm() < 100) {
                lista += String.format("%d | %5d | %s  \n", v.getPlaca(), v.getKm(), v.getCliente());
            }
        }
        return lista;
    }

}
