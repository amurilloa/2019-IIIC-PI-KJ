/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciouno;

/**
 *
 * @author Allan Murillo
 */
public class Vehiculo {

    private int placa;
    private String cliente;
    private int km;

    public Vehiculo() {
    }

    public Vehiculo(int placa, String cliente, int km) {
        this.placa = placa;
        this.cliente = cliente;
        this.km = km;
    }

    public int getPlaca() {
        return placa;
    }

    public void setPlaca(int placa) {
        this.placa = placa;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public int getKm() {
        return km;
    }

    public void setKm(int km) {
        this.km = km;
    }

    @Override
    public String toString() {
        return "Vehiculo{" + "placa=" + placa + ", cliente=" + cliente + ", km=" + km + '}';
    }
}
