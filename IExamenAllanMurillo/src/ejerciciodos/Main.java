/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciodos;

import util.Util;

/**
 *
 * @author Allan Murillo
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();
        int[] arr = {1, 2, 3, 4, 3, 4, 6, 7, 8};

        String menu = "1. Generar Arreglo\n"
                + "2. Multiplicar Pares e Impares\n"
                + "3. Lotería\n"
                + "4. Salir";

        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    int n1 = Util.leerInt("Número 1");
                    int n2 = Util.leerInt("Número 2");
                    arr = log.generar(n1, n2);
                    Util.mostrar(log.imprimir(arr));
                    arr = log.generar(n2, n1);
                    Util.mostrar(log.imprimir(arr));
                    break;
                case 2:
                    Util.mostrar(log.multiplicar(arr));
                    break;
                case 3:
                    int boleto = Util.leerInt("Número a jugar 0-99");
                    Util.mostrar(log.loteria(boleto) ? "Ganó" : "Perdió");
                    break;
                case 4:
                    break APP;
            }
        } while (true);
    }
}
