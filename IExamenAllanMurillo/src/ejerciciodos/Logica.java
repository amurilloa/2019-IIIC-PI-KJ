/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciodos;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    public int[] generar(int n1, int n2) {
        int tam = Math.abs(n1 - n2) + 1;
        int[] nuevo = new int[tam];

        if (n1 > n2) {
            for (int i = 0; i < nuevo.length; i++) {
                nuevo[i] = n1--;
            }
        } else {
            for (int i = 0; i < nuevo.length; i++) {
                nuevo[i] = n1++;
            }
        }
        return nuevo;
    }

    public String imprimir(int[] arreglo) {
        String txt = "";
        for (int i : arreglo) {
            txt += i + " ";
        }
        return txt;
    }

    public String multiplicar(int[] arreglo) {
        String txt = "";
        String pares = "(";
        String impares = "(";
        int par = 0;
        int impar = 0;

        for (int num : arreglo) {
            if (num % 2 == 0) {
                pares += num + "+";
                par += num;
            } else {
                impares += num + "+";
                impar += num;
            }
        }
        pares = pares.substring(0, pares.length() - 2) + ")";
        impares = impares.substring(0, impares.length() - 2) + ")";
        int total = par * impar;
        return pares + "*" + impares + "=" + total;
    }

    public boolean loteria(int num) {
        int ale = (int) (Math.random() * 100);
        System.out.println(ale);
        return ale == num;
    }
}
