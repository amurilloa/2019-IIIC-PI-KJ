/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cronometro;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Cronometro {

    private int x;
    private int y;
    private int seg;
    private Numero[] numeros;
    private int estilo;

    public Cronometro() {
        numeros = new Numero[6];
        configurar();
    }

    private void configurar() {
        int vx = 143;
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = new Numero(x + vx, y + 280, 0);
            vx += i != 0 && i % 2 != 0 ? 160 : 115;
        }
    }

    public void pintar(Graphics g) {
        g.setColor(Color.ORANGE);
        g.fillArc(x + 25, y + 25, 1000, 700, 90, calcGrados());

        g.setColor(Color.BLACK);
        g.fillOval(x + 35, y + 35, 980, 680);
        for (int i = 0; i < numeros.length; i++) {
            numeros[i].setEstilo(estilo);
            numeros[i].pintar(g);
        }
        g.setColor(estilo == 0 ? Color.GREEN : Color.CYAN);
        g.fillOval(x + 380, y + 340, 16, 16);
        g.fillOval(x + 655, y + 340, 16, 16);
        g.fillOval(x + 380, y + 400, 16, 16);
        g.fillOval(x + 655, y + 400, 16, 16);

    }

    public void addSeg() {
        seg++;
        calcularTiempo();
    }

    private void calcularTiempo() {
        int st = seg % 60;
        numeros[5].setNumero(st % 10);
        numeros[4].setNumero(st / 10);
        int min = seg / 60;
        int mt = min % 60;
        numeros[3].setNumero(mt % 10);
        numeros[2].setNumero(mt / 10);
        int hrs = min / 60;
        numeros[1].setNumero(hrs % 10);
        numeros[0].setNumero(hrs / 10);
    }

    public void setEstilo(int estilo) {
        this.estilo = estilo;
    }

    public void cambiarSombra() {
        for (int i = 0; i < numeros.length; i++) {
            numeros[i].cambiarSombra();
        }
    }

    private int calcGrados() {
        int st = seg % 60;
        if (st == 0) {
            return -360;
        }
        return -360 * st / 60;
    }

}
