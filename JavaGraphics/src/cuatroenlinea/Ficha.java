/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuatroenlinea;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Ficha {

    private int y;
    private int dir;
    private final int x;
    private final int numJug;
    private final Color color;

    public Ficha(int numJug, int x, int y) {
        color = numJug == 1 ? Color.BLUE : Color.YELLOW;
        dir = 1;
        this.numJug = numJug;
        this.x = x;
        this.y = y;
    }

    public void pintar(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, 92, 92);
    }

    public void mover() {
        if (dir > 0) {
            y += 3;
        }
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public int getNumJug() {
        return numJug;
    }

    public Color getColor() {
        return color;
    }

    public int getY() {
        return y;
    }
}
