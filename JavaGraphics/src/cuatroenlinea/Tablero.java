/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuatroenlinea;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Tablero {

    private final Ficha[][] matriz;
    
    private final int x;
    private final int y;
    private int pos; //0-6 

    public Tablero() {
        x = 100;
        y = 80;
        pos = 0;
        matriz = new Ficha[6][7];
    }

    public void pintar(Graphics g) {
        g.setColor(Color.DARK_GRAY);
        g.fillRect(x, y, 700, 600);

        for (int f = 0; f < matriz.length; f++) {
            for (int c = 0; c < matriz[f].length; c++) {
                //Hueco 
                g.setColor(Color.WHITE);
                g.fillOval(x + 5 + (c * 100), y + 5 + (f * 100), 90, 90);

                //Si hay una ficha la pintamos
                if (matriz[f][c] != null) {
                    matriz[f][c].pintar(g);
                }
            }
        }

        //Indicador
        int[] xs = {x + 25 + (pos * 100), x + 50 + (pos * 100), x + 75 + (pos * 100)};
        int[] ys = {y - 70, y - 20, y - 70};
        g.setColor(Color.RED);
        g.fillPolygon(xs, ys, xs.length);

        Font f = new Font("Arial", Font.BOLD, 20);
        g.setFont(f);
        g.setColor(Color.WHITE);
        for (int i = 0; i < 7; i++) {
            g.drawString(String.valueOf(i + 1), x + 43 + (i * 100), y - 42);
        }
    }

    public void cambiarPos(int dir) {
        pos += dir;
        if (pos < 0 || pos > 6) {
            pos -= dir;
        }
    }

    public int soltarFicha(int numJug) {
        return x + 5 + (pos * 100);
    }

    public int detenerFicha(Ficha ficha) {
        int columna = pos;
        int lim = -1;
        int fila = 0;
        for (int fil = matriz.length - 1; fil >= 0; fil--) {
            if (matriz[fil][columna] == null) {
                lim = fil * 100 + 83;
                fila = fil;
                break;
            }
        }
        if(lim<0){
            return 0;
        }
        //Detenerla
        System.out.println(fila);
        if (ficha.getY() >= lim) {
            ficha.setDir(0);
            matriz[fila][columna] = ficha;
            return 1;
        }
        return 2;
    }
}
