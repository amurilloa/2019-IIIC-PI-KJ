/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cuatroenlinea;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel implements KeyListener {

    private Ficha ficha;
    private int jugActual;
    private boolean soltar;
    private final Tablero tablero;

    public MiPanel() {
        setBackground(Color.BLACK);
        setFocusable(true);
        addKeyListener(this);
        tablero = new Tablero();
        jugActual = 1;
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(900, 700);
    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int cX = getWidth() / 2;
        int cY = getHeight() / 2;
        Font f;

        tablero.pintar(g);
        if (soltar) {
            ficha.pintar(g);
            int r = tablero.detenerFicha(ficha);
            soltar = r == 2; //Sigue el movimiento 
            ficha.mover();
            if (r == 1) { //Coloco la ficha
                //if(tablero.gano()){
                //  gano = true;
                //}
                jugActual = jugActual == 1 ? 2 : 1;
            }
        }
        g.setColor(Color.RED);
        g.drawLine(0, cY, getWidth(), cY);
        g.drawLine(cX, 0, cX, getHeight());
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_LEFT && !soltar) {
            tablero.cambiarPos(-1);
        } else if (ke.getKeyCode() == KeyEvent.VK_RIGHT && !soltar) {
            tablero.cambiarPos(1);
        } else if (ke.getKeyCode() == KeyEvent.VK_SPACE && !soltar) {
            soltar = true;
            ficha = new Ficha(jugActual, tablero.soltarFicha(jugActual), -95);
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
