/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movimiento;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Bola {

    private Color color;
    private int x;
    private int y;
    private int dir;
    private final int TAMANO = 36;
    private final int SPEED = 2;

    public Bola() {
    }

    public Bola(Color color, int x, int y) {
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public void pintar(Graphics g) {
        g.setColor(color);
        g.fillOval(x, y, TAMANO, TAMANO);
    }

    public void mover(int ancho, int alto) {
        switch (dir) {
            case 1:
                y -= SPEED;
                break;
            case 2:
                y -= SPEED;
                x += SPEED;
                break;
            case 3:
                x += SPEED;
                break;
            case 4:
                y += SPEED;
                x += SPEED;
                break;
            case 5:
                y += SPEED;
                break;
            case 6:
                y += SPEED;
                x -= SPEED;
                break;
            case 7:
                x -= SPEED;
                break;
            case 8:
                x -= SPEED;
                y -= SPEED;
                break;
        }

        if (x <= 0) {
            dir = cambioDir(7);
        } else if (x + TAMANO >= ancho) {
            dir = cambioDir(3);
        } else if (y <= 0) {
            dir = cambioDir(1);
        } else if (y + TAMANO >= alto) {
            dir = cambioDir(5);
        }
       
        x = x <= 0 ? 1 : x;
        y = y <= 0 ? 1 : y;

        x = x + TAMANO >= ancho ? ancho - TAMANO - 1 : x;
        y = y + TAMANO >= alto ? alto - TAMANO - 1 : y;

    }

    private int cambioDir(int pared) {
        int r = (int) (Math.random() * 3);
        if (dir == 1) {
            int[] pos = {2, 3, 4};
            return pos[r];
        } else if (dir == 2) {
            if (pared == 1) {
                int[] pos = {4, 5, 4};
                return pos[r];
            } else {
                int[] pos = {8, 7, 8};
                return pos[r];
            }
        } else if (dir == 3) {
            int[] pos = {1, 7, 6};
            return pos[r];
        } else if (dir == 4) {
            if (pared == 3) {
                int[] pos = {6, 7, 6};
                return pos[r];
            } else {
                int[] pos = {2, 1, 2};
                return pos[r];
            }
        } else if (dir == 5) {
            int[] pos = {8, 1, 2};
            return pos[r];
        } else if (dir == 6) {
            if (pared == 7) {
                int[] pos = {4, 3, 4};
                return pos[r];
            } else {
                int[] pos = {8, 1, 8};
                return pos[r];
            }
        } else if (dir == 7) {
            int[] pos = {2, 3, 4};
            return pos[r];
        } else {
            if (pared == 7) {
                int[] pos = {2, 3, 2};
                return pos[r];
            } else {
                int[] pos = {6, 5, 6};
                return pos[r];
            }
        }
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

}
