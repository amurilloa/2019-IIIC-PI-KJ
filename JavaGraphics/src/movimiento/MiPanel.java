/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package movimiento;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel implements KeyListener {

    private Bola b1;
    private int cX;
    private int cY;
    private int tiempo;

    public MiPanel() {
        b1 = new Bola(Color.BLUE, 232, 430);
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(500, 700);
    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        cX = getWidth() / 2;
        cY = getHeight() / 2;

        b1.pintar(g2);
        b1.mover(getWidth(), getHeight());

        tiempo += 10;
        g.drawString(String.valueOf(tiempo / 1000), 475, 25);

        g.setColor(Color.red);
        g.drawLine(0, cY, getWidth(), cY); //x
        g.drawLine(cX, 0, cX, getHeight()); //y

    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_S) {
            b1.setDir(0);
        } else if (ke.getKeyCode() == KeyEvent.VK_SPACE) {
            b1.setDir(5);
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
