/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package choques;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel implements KeyListener {

    private int cX;
    private int cY;
    private boolean pausa;
    private boolean salir;
    private boolean jugando;

    private Bola bola;
    private Raqueta raqueta;

    public MiPanel() {
        bola = new Bola(Color.ORANGE, 232, 644);
        raqueta = new Raqueta(210, 680, 0);
        config();
    }

    private void config() {
        setBackground(Color.BLACK);
//        setBorder(new LineBorder(Color.WHITE, 3));
        setFocusable(true);
        addKeyListener(this);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(500, 700);
    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        Font f;
        cX = getWidth() / 2;
        cY = getHeight() / 2;

        if (salir) {
            g.setColor(Color.WHITE);
            f = new Font("Arial", Font.BOLD, 28);
            g.setFont(f);
            g.drawString("EXIT", 205, 310);
            f = new Font("Arial", Font.BOLD, 20);
            g.setFont(f);
            g.drawString("<< Press Y for exit or N for resume >>", 65, 340);
        } else {
            g.setColor(Color.WHITE);
            f = new Font("Arial", Font.BOLD, 14);
            g.setFont(f);
            g.drawString("(E)xit", getWidth() - 50, 20);

            if (!jugando) {
                bola.setX(raqueta.getX() + 22);
                g.setColor(Color.WHITE);
                f = new Font("Arial", Font.BOLD, 28);
                g.setFont(f);
                g.drawString("PLAY", 205, 310);
                f = new Font("Arial", Font.BOLD, 24);
                g.setFont(f);
                g.drawString("<< Press SPACE for play >>", 95, 340);
            }

            bola.pintar(g2);
            raqueta.pintar(g2);

            if (!pausa) {
                bola.mover(getWidth(), getHeight());
                raqueta.mover(getWidth());
                bola.choco(raqueta);
                if (bola.perdio(getHeight())) {
                    bola = new Bola(Color.ORANGE, 232, 644);
                    raqueta = new Raqueta(210, 680, 0);
                    jugando = false;
                }
            } else if (jugando) {
                g.setColor(Color.WHITE);
                f = new Font("Arial", Font.BOLD, 28);
                g.setFont(f);
                g.drawString("PAUSE", 200, 310);
                f = new Font("Arial", Font.BOLD, 24);
                g.setFont(f);
                g.drawString("<< Press P for resume >>", 105, 340);
            }
        }

        g.setColor(Color.red);
        g.drawLine(0, cY, getWidth(), cY); //x
        g.drawLine(cX, 0, cX, getHeight()); //y

    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_LEFT) {
            raqueta.setDir(7);
        } else if (ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            raqueta.setDir(3);
        } else if (ke.getKeyCode() == KeyEvent.VK_SPACE && !jugando) {
            jugando = true;
            bola.setDir(1);
        } else if (ke.getKeyCode() == KeyEvent.VK_P && jugando) {
            pausa = !pausa;
        } else if (ke.getKeyCode() == KeyEvent.VK_E) {
            salir = true;
        } else if (salir && ke.getKeyCode() == KeyEvent.VK_Y) {
            System.exit(0);
        } else if (salir && ke.getKeyCode() == KeyEvent.VK_N) {
            salir = false;
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_LEFT
                || ke.getKeyCode() == KeyEvent.VK_RIGHT) {
            raqueta.setDir(0);
        }
    }

}
