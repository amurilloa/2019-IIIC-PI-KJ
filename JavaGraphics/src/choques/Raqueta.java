/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package choques;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

/**
 *
 * @author Allan Murillo
 */
public class Raqueta {

    private int x;
    private int y;
    private int dir;

    private final int SPEED = 3;

    public Raqueta() {
    }

    public Raqueta(int x, int y, int dir) {
        this.x = x;
        this.y = y;
        this.dir = dir;
    }

    public void pintar(Graphics g) {
        g.setColor(Color.BLUE);
        g.fillRect(x, y, 20, 20);
        g.setColor(Color.GRAY);
        g.fillRect(x + 20, y, 40, 20);
        g.setColor(Color.BLUE);
        g.fillRect(x + 60, y, 20, 20);
        //TODO: Borrar - Bounds
        g.setColor(Color.YELLOW);
        g.drawRect(x, y, 20, 20);
        g.drawRect(x + 20, y, 40, 20);
        g.drawRect(x + 60, y, 20, 20);
    }

    public Rectangle getBounds(int lado) {
        if (lado == 1) {
            return new Rectangle(x, y, 20, 20);
        } else if (lado == 3) {
            return new Rectangle(x + 60, y, 20, 20);
        } else {
            return new Rectangle(x + 20, y, 40, 20);
        }
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public void mover(int ancho) {
        if (dir == 3) {
            x += SPEED;
        } else if (dir == 7) {
            x -= SPEED;
        }
        x = x <= 0 ? 1 : x;
        x = x + 80 >= ancho ? ancho - 81 : x;
    }

    public int getX() {
        return x;
    }
}
