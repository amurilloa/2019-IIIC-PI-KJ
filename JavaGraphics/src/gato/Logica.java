/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gato;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    private final char[][] tablero;

    private int fila;
    private int columna;
    private String jugadorUno;
    private String jugadorDos;
    private boolean turno;
    private boolean reiniciar;

    private final char MARCA_J1 = 'X';
    private final char MARCA_J2 = 'O';

    private char linea;

    public Logica() {
        fila = 1;
        columna = 1;
        jugadorUno = "Jugador 1";
        jugadorDos = "Jugador 2";
        tablero = new char[3][3];
        limpiar();
        rifar();
    }

    public void reiniciar() {
        limpiar();
        rifar();
    }

    /**
     * Rifa el jugador inicial a partir de un número aleatorio
     */
    private void rifar() {
        int ale = (int) (Math.random() * 10) + 1;
        turno = ale <= 5;
    }

    /**
     * Reemplazar los caracteres de la matriz(tablero) por un guión bajo(_)
     */
    private void limpiar() {
        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {
                tablero[f][c] = '_';
            }
        }
    }

    public void imprimir(Graphics g) {
        for (int f = 0; f < tablero.length; f++) {
            for (int c = 0; c < tablero[f].length; c++) {
                g.setColor(Color.WHITE);
                g.fillRect(12 + c * 92, 108 + f * 92, 90, 90);

                Font font = new Font("DUMMY", Font.BOLD, 80);
                g.setFont(font);

                if (tablero[f][c] == MARCA_J1) {
                    g.setColor(Color.BLUE);
                    g.drawString("X", 26 + c * 92, 182 + f * 92);
                } else if (tablero[f][c] == MARCA_J2) {
                    g.setColor(Color.RED);
                    g.drawString("O", 26 + c * 92, 182 + f * 92);
                }
            }
        }

        Font font = new Font("DUMMY", Font.BOLD, 14);
        g.setFont(font);
        if (gano()) {
            reiniciar = true;
            g.setColor(Color.WHITE);
            g.drawString("Ganador" + getGanador(), 45, 26);
            dibujarLinea(g);
        } else if (empate()) {
            reiniciar = true;
            g.setColor(Color.WHITE);
            g.drawString("Partida Empatada", 85, 26);
        } else {
            //Jugador actual
            g.setColor(Color.WHITE);
            g.drawString("Jugador Actual" + getJugadorActual(), 45, 26);

            //Cursor
            int f = fila - 1;
            int c = columna - 1;
            int[] xs = {26 + c * 90, 26 + c * 90, 16 + c * 90, 10 + c * 90, 4 + c * 90, 10 + c * 90, 0 + c * 90};
            int[] ys = {182 + f * 90, 215 + f * 90, 210 + f * 90, 221 + f * 90, 217 + f * 90, 205 + f * 90, 201 + f * 90};
            g.setColor(Color.RED);
            g.fillPolygon(xs, ys, xs.length);
        }
    }

    /**
     * Marca la casilla correspondiente dependiendo del turno del jugador
     */
    public void marcar() {
        if (reiniciar) {
            fila = 1;
            columna = 1;
            limpiar();
            rifar();
            reiniciar = false;
        } else if (fila > 0 && fila < 4 && columna > 0 && columna < 4) { //marca este dentro del tablero
            if (tablero[fila - 1][columna - 1] == '_') { //La casilla este vacía
                tablero[fila - 1][columna - 1] = turno ? MARCA_J1 : MARCA_J2; //Marco dependiendo del turno
                turno = !turno;//invertimos el turno para el cambio correspondiente
            }
        }
    }

    public boolean gano() {
        //3 Filas
        for (int f = 0; f < 3; f++) {
            if (tablero[f][0] != '_' && tablero[f][0] == tablero[f][1] && tablero[f][0] == tablero[f][2]) {
                linea = 'H';
                return true;
            }
        }
        //3 Columnas
        for (int c = 0; c < 3; c++) {
            if (tablero[0][c] != '_' && tablero[0][c] == tablero[1][c] && tablero[0][c] == tablero[2][c]) {
                linea = 'V';
                return true;
            }
        }
        //Diagonales
        if (tablero[0][0] != '_' && tablero[0][0] == tablero[1][1] && tablero[0][0] == tablero[2][2]) {
            linea = 'D';
            return true;
        }
        if (tablero[0][2] != '_' && tablero[0][2] == tablero[1][1] && tablero[0][2] == tablero[2][0]) {
            linea = 'd';
            return true;
        }
        return false;
    }

    public boolean empate() {
        for (char[] fila : tablero) {
            for (char col : fila) {
                if (col == '_') {
                    return false;
                }
            }
        }
        return true;
    }

    public void setJugadorUno(String jugadorUno) {
        this.jugadorUno = jugadorUno;
    }

    public void setJugadorDos(String jugadorDos) {
        this.jugadorDos = jugadorDos;
    }

    public String getJugadorActual() {
        return turno ? jugadorUno + "(" + MARCA_J1 + ")" : jugadorDos + "(" + MARCA_J2 + ")";
    }

    public String getGanador() {
        return turno ? jugadorDos : jugadorUno;
    }

    public void cambiarF(int f) {
        if (!reiniciar) {
            fila += f;
            if (fila < 1 || fila > 3) {
                fila -= f;
            }
        }
    }

    public void cambiarC(int c) {
        if (!reiniciar) {
            columna += c;
            if (columna < 1 || columna > 3) {
                columna -= c;
            }
        }
    }

    private void dibujarLinea(Graphics g) {
        g.setColor(!turno ? Color.BLUE : Color.RED);
        if (linea == 'H') {
            g.fillRect(26, 145 + (fila - 1) * 92, 235, 15);
        } else if (linea == 'V') {
            g.fillRect(45 + (columna - 1) * 92, 115, 15, 245);
        } else if (linea == 'D') {
            int[] xs = {26, 41, 276, 261};
            int[] ys = {124, 124, 375, 375};
            g.fillPolygon(xs, ys, xs.length);
        } else if (linea == 'd') {
            int[] xs = {26, 41, 276, 261};
            int[] ys = {375, 375, 124, 124};
            g.fillPolygon(xs, ys, xs.length);
        }

    }
}
