/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numeros;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author Allan Murillo
 */
public class Numero {

    private int num;
    private boolean sombra;
    private int estilo;

    private final int x;
    private final int y;
    private final Color SOMBRA = new Color(39, 39, 39);

    public Numero() {
        x = 100;
        y = 100;
        num = 0;
        sombra = false;
        estilo = 1;
    }

    public Numero(int x, int y, int num) {
        this.x = x;
        this.y = y;
        this.num = num;
    }

    public void pintar(Graphics2D g) {
        if (estilo == 0) {
            estiloDefecto(g);
        } else if (estilo == 1) {
            estiloLED(g);
        }
    }

    private int[] inc(int[] arr, int can) {
        int[] temp = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            temp[i] = arr[i] + can;
        }
        return temp;
    }

    public void setNumero(int numero) {
        if (numero >= 0 && numero <= 9) {
            this.num = numero;
        }
    }

    private void estiloDefecto(Graphics2D g) {
        g.setColor(SOMBRA);
        //Horizontal
        int[] xh = {x + 5, x + 10, x + 85, x + 90, x + 85, x + 10};
        int[] yh = {y + 5, y + 0, y + 0, y + 5, y + 10, y + 10};

        //Vertical
        int[] yv = {5, 10, 85, 90, 85, 10};
        int[] xv = {5, 0, 0, 5, 10, 10};
        xv = inc(xv, x);
        yv = inc(yv, y);

        if (sombra) {
            g.fillPolygon(inc(xh, 2), yh, xh.length);
            g.fillPolygon(xv, inc(yv, 2), xh.length);
            g.fillPolygon(inc(xv, 90), inc(yv, 2), xh.length);
            g.fillPolygon(inc(xh, 2), inc(yh, 90), xh.length);
            g.fillPolygon(xv, inc(yv, 92), xh.length);
            g.fillPolygon(inc(xv, 90), inc(yv, 92), xh.length);
            g.fillPolygon(inc(xh, 2), inc(yh, 180), xh.length);
        }
        g.setColor(Color.GREEN);

        //Arriba 
        if (num != 1 && num != 4) {
            g.fillPolygon(inc(xh, 2), yh, xh.length);
        }

        //Izq-Arr
        if (num != 1 && num != 2 && num != 3 && num != 7) {
            g.fillPolygon(xv, inc(yv, 2), xh.length);
        }

        //Der-Arr
        if (num != 5 && num != 6) {
            g.fillPolygon(inc(xv, 90), inc(yv, 2), xh.length);
        }

        //Medio
        if (num != 0 && num != 1 && num != 7) {
            g.fillPolygon(inc(xh, 2), inc(yh, 90), xh.length);
        }

        //Izq-Aba
        if (num != 1 && num != 3 && num != 4 && num != 5 && num != 7 && num != 9) {
            g.fillPolygon(xv, inc(yv, 92), xh.length);
        }
        //Der-Aba
        if (num != 2) {
            g.fillPolygon(inc(xv, 90), inc(yv, 92), xh.length);
        }

        //Aba
        if (num != 1 && num != 4 && num != 7) {
            g.fillPolygon(inc(xh, 2), inc(yh, 180), xh.length);
        }
    }

    private void estiloLED(Graphics2D g) {
        int[][] leds = new int[9][5];

        //Arriba 
        if (num != 1 && num != 4) {
            lineaON(0, leds);
        }

        //Izq-Arr
        if (num != 1 && num != 2 && num != 3 && num != 7) {
            columnaON(0, 0, leds);
        }

        //Der-Arr
        if (num != 5 && num != 6) {
            columnaON(0, 4, leds);
        }

        //Medio
        if (num != 0 && num != 1 && num != 7) {
            lineaON(4, leds);
        }

        //Izq-Aba
        if (num != 1 && num != 3 && num != 4 && num != 5 && num != 7 && num != 9) {
            columnaON(4, 0, leds);
        }
        //Der-Aba
        if (num != 2) {
            columnaON(4, 4, leds);
        }

        //Aba
        if (num != 1 && num != 4 && num != 7) {
            lineaON(8, leds);
        }

        for (int f = 0; f < leds.length; f++) {
            for (int c = 0; c < leds[f].length; c++) {
                g.setColor(leds[f][c] == 1 ? Color.CYAN : sombra ? SOMBRA : Color.BLACK);
                g.fillOval(0 + x + c * 21, 0 + y + f * 22, 16, 16);
            }
        }
    }

    private void lineaON(int fila, int[][] leds) {
        for (int j = 0; j < leds[fila].length; j++) {
            leds[fila][j] = 1;
        }
    }

    private void columnaON(int f, int col, int[][] leds) {
        for (int j = f; j < f + 5; j++) {
            leds[j][col] = 1;
        }
    }

    public void cambiarSombra() {
        sombra = !sombra;
    }

    public void setEstilo(int estilo) {
        this.estilo = estilo;
    }

}
