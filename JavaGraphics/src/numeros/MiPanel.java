/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package numeros;

import gato.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel implements KeyListener {

    private Numero num;
    private Numero num2;

    public MiPanel() {
        setBackground(Color.BLACK);
        setFocusable(true);
        addKeyListener(this);
        num = new Numero();
        num2 = new Numero(100, 100, 8);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(300, 400);
    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int cX = getWidth() / 2;
        int cY = getHeight() / 2;
        Font f;
        num.pintar(g);

//        g.setColor(Color.RED);
//        g.drawLine(0, cY, getWidth(), cY);
//        g.drawLine(cX, 0, cX, getHeight());
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        num.setNumero(ke.getKeyCode() - 48);
        if (ke.getKeyCode() == KeyEvent.VK_S) {
            num.cambiarSombra();
        } else if (ke.getKeyCode() == KeyEvent.VK_L) {
            num.setEstilo(1);
        } else if (ke.getKeyCode() == KeyEvent.VK_D) {
            num.setEstilo(0);
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

}
