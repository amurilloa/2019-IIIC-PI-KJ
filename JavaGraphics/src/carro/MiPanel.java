/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carro;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel {

    private Carro c;
    private Carro c2;

    public MiPanel() {
        c = new Carro(0, 0, Color.BLUE, Color.BLACK, true);
        c2 = new Carro(200, 200, Color.RED, Color.BLACK, false);
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(500, 700);

    }

    @Override
    public void paint(Graphics g2) {
        super.paint(g2);
        Graphics2D g = (Graphics2D) g2;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        int centroX = getWidth() / 2;
        int centroY = getHeight() / 2;
        c.pintar(g2);
        if (c.getX() >= getWidth()) {
            c.setX(-200);
            c.setY((int) (Math.random() * getHeight()));
        }
        c.setX(c.getX() + 2);
        c2.pintar(g2);
        c2.setX(c2.getX() + 1);
        g.setColor(Color.red);
        g.drawLine(0, centroY, getWidth(), centroY); //x
        g.drawLine(centroX, 0, centroX, getHeight()); //y

    }

}
