/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carro;

import java.awt.Color;
import java.awt.Graphics;

/**
 *
 * @author Allan Murillo
 */
public class Carro {

    private int x;
    private int y;
    private Color colorP;
    private Color colorS;
    private boolean encendido; // 1 ->, <- 2

    public Carro(int x, int y, Color colorP, Color colorS, boolean encendido) {
        this.x = x;
        this.y = y;
        this.colorP = colorP;
        this.colorS = colorS;
        this.encendido = encendido;
    }

    public void pintar(Graphics g) {
        //Carrocería
        g.setColor(colorP);
        int[] xs = {x + 30, x + 55, x + 65, x + 125, x + 155, x + 195, x + 195, x + 30};
        int[] ys = {y + 20, y + 20, y + 0, y + 0, y + 20, y + 25, y + 45, y + 45};
        g.fillPolygon(xs, ys, xs.length);
        //Llantas
        g.setColor(colorS);
        g.fillOval(x + 55, y + 32, 26, 26);
        g.fillOval(x + 155, y + 32, 26, 26);
        //Mufla 
        g.setColor(Color.GRAY);
        g.fillRect(x + 25, y + 42, 15, 5);

        //Luces 
        g.setColor(!encendido ? Color.GRAY : Color.YELLOW);
        g.fillRect(x + 190, y + 26, 5, 5);
        g.setColor(Color.RED);
        g.fillRect(x + 31, y + 21, 5, 10);

        if (encendido) {
            g.setColor(Color.GRAY);
            g.fillOval(x + 15, y + 42, 10, 5);
            g.fillOval(x + 10, y + 38, 10, 5);
        }
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }

}
