/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javagraphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import javax.swing.JPanel;

/**
 *
 * @author Allan Murillo
 */
public class MiPanel extends JPanel {

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(300, 400);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        //Pintamos nosotros
        g2.setColor(Color.BLUE);
        g2.drawLine(150, 0, 150, 400);
        g2.fillRect(0, 199, 300, 3);
        g2.fillOval(100, 150, 100, 100);
        g2.setColor(Color.RED);

        g2.drawOval(140, 190, 20, 20);

        g2.fillArc(50, 50, 100, 100, 45, 90);
        g2.setColor(Color.RED);
        g2.fillArc(50, 50, 100, 100, 135, 90);
        g2.setColor(new Color(0, 128, 255));
        g2.fillArc(50, 50, 100, 100, 225, 180);

        int[] xs = {180, 260, 220};
        int[] ys = {320, 320, 240};
        g2.fillPolygon(xs, ys, xs.length);

    }

}
