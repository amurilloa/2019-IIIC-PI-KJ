/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoarchivos;

import java.text.SimpleDateFormat;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    private final Persona[] personas;
    private final ManejadorArchivos ma;
    private final String RUTA = "bd/datos.txt";

    public Logica() {
        ma = new ManejadorArchivos();
        personas = new Persona[100];
        cargarEncuestas();
    }

    public void registrarEncuesta(Persona p) {
        for (int i = 0; i < personas.length; i++) {
            if (personas[i] == null) {
                p.setId(i);
                personas[i] = p;
                break;
            }
        }
        guardar();
    }

    public String imprimir() {
        String texto = "";
        for (Persona persona : personas) {
            if (persona != null) {
                texto += persona + "\n";
            }
        }
        return texto;
    }

    private void cargarEncuestas() {
        String datos = ma.leerTextoArchivo(RUTA);
        String[] res = datos.split("\n");
        for (String pTexto : res) {
            String[] valores = pTexto.split(",");
            Persona p = new Persona();
            p.setId(Integer.parseInt(valores[0]));
            p.setCedula(Integer.parseInt(valores[1]));
            p.setNombre(valores[2]);
            p.setGenero(valores[3].charAt(0));
            p.setFechaNacimiento(Util.textoFecha(valores[4]));
            personas[p.getId()] = p;
        }
    }

    public String mostrarEstadisticas() {
        String txt = "Total: %d\n"
                + "Total Hombres: %d(%.1f%%)\n"
                + "Total Mujeres: %d(%.1f%%)";

        int hom = 0;
        int muj = 0;
        for (Persona persona : personas) {
            if (persona != null) {
                if ('M' == persona.getGenero()) {
                    hom++;
                } else {
                    muj++;
                }

            }
        }
        int total = hom + muj;
        return String.format(txt, total, hom, hom * 100.0 / total, muj, muj * 100.0 / total);
    }

    public void eliminar(int id) {
        personas[id] = null;
        guardar();
    }

    private void guardar() {
        String datos = imprimir();
        ma.escribirTextoArchivo(RUTA, datos);
    }
}
