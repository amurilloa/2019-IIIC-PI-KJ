/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoarchivos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class Util {

    public static final String TITULO = "DenoArchivos - UTN v0.1";

    public static void mostrar(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje, TITULO,
                JOptionPane.INFORMATION_MESSAGE);
    }

    public static boolean confirmar(String mensaje) {
        int res = JOptionPane.showConfirmDialog(null, mensaje, TITULO,
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
        return res == JOptionPane.YES_OPTION;
    }

    public static String seleccionar(String mensaje, String[] opciones) {
        String op = (String) JOptionPane.showInputDialog(null, mensaje, TITULO,
                JOptionPane.QUESTION_MESSAGE, null, opciones, opciones[0]);
        return op;
    }

    public static int leerInt(String mensaje) {
        String error = "";
        while (true) {
            try {
                int op = Integer.parseInt(JOptionPane.showInputDialog(null, error
                        + mensaje, TITULO, JOptionPane.QUESTION_MESSAGE));
                return op;
            } catch (Exception e) {
                error = "Formato inválido,\n\n";
            }
        }
    }

    public static double leerNum(String mensaje) {
        String error = "";
        while (true) {
            try {
                double op = Double.parseDouble(JOptionPane.showInputDialog(null, error
                        + mensaje, TITULO, JOptionPane.QUESTION_MESSAGE));
                return op;
            } catch (Exception e) {
                error = "Formato inválido,\n\n";
            }
        }
    }

    public static String leerTexto(String mensaje) {
        String op = JOptionPane.showInputDialog(null, mensaje, TITULO,
                JOptionPane.QUESTION_MESSAGE);
        return op;

    }

    public static Date textoFecha(String fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            return sdf.parse(fecha);
        } catch (ParseException ex) {
            return new Date();
        }
    }
}
