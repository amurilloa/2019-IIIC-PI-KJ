/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package demoarchivos;

import java.util.Date;

/**
 *
 * @author Allan Murillo
 */
public class DemoArchivos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Logica log = new Logica();
        String menu = "1. Aplicar encuesta\n"
                + "2. Ver resultados\n"
                + "3. Borrar\n"
                + "4. Salir";
        
        String[] generos = {"Masculino", "Femenino"};

        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    Persona p = new Persona();
                    p.setCedula(Util.leerInt("Cédula"));
                    p.setNombre(Util.leerTexto("Nombre"));
                    p.setFechaNacimiento(new Date());
                    p.setGenero(Util.seleccionar("Género", generos).charAt(0));
                    log.registrarEncuesta(p);
                    break;
                case 2:
                    Util.mostrar(log.mostrarEstadisticas());
                    break;
                case 3:
                    int id = Util.leerInt("Digite el ID de la encuesta a eliminar\n" + log.imprimir());
                    if(Util.confirmar("Esta seguro que desea eliminar la encuesta con el ID: " + id)){
                        log.eliminar(id);
                    }
                    break;
                case 4:
                    break APP;
            }
        } while (true);

        //- Relativo vs Absoluto
        //  Cambiar      Fijo     --> Rutas Relativas y Rutas Absolutas
        //                              bd/datos.txt    c:/proyecto1/datos.txt
        // D:\Proyectos\2019-IIIC-PI-KJ\DemoArchivos\bd\datos.txt
    }
}
