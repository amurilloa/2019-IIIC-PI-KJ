/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();
        System.out.println(log.cajero(888));
        int precio = Integer.parseInt(JOptionPane.showInputDialog("Precio del crucero por persona"));
        int cantidad = Integer.parseInt(JOptionPane.showInputDialog("Cantidad de personas"));
        int edad = 0;
        if (cantidad == 1) {
            edad = Integer.parseInt(JOptionPane.showInputDialog("Edad de la Persona"));
        }
        double desc = log.crucero(precio, cantidad, edad);
        JOptionPane.showMessageDialog(null, String.format("El descuento para %d personas es de $%.2f", cantidad, desc));
    }
}
