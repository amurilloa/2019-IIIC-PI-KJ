/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tarea;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    public String cajero(int monto) {
        String detalle = "";

        int mon = 500;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de %d\n", can, mon);
        }

        mon = 200;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de %d\n", can, mon);
        }

        mon = 100;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de %d\n", can, mon);
        }

        mon = 50;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de %d\n", can, mon);
        }

        mon = 20;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de %d\n", can, mon);
        }

        mon = 10;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de %d\n", can, mon);
        }

        mon = 5;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d billetes de %d\n", can, mon);
        }

        mon = 2;
        if (monto >= mon) {
            int can = monto / mon;
            monto %= mon;
            detalle += String.format("%d monedas de %d\n", can, mon);
        }

        if (monto >= 0) {
            detalle += "1 moneda de 1";
        }

        return detalle;
    }

    public double crucero(int precio, int cantidad, int edad) {
        double desc = 0;
        if (cantidad <= 0) {
            return -1;
        }

        if (cantidad == 1) {
            if (edad >= 18 && edad <= 30) {
                desc = precio * 0.078;
            } else if (edad > 30) {
                desc = precio * 0.10;
            }
        } else if (cantidad == 2) {
            desc = (precio * 0.115) * cantidad;
        } else if (cantidad > 3) {
            desc = (precio * 0.15) * cantidad;
        }

        return desc;
    }
}
