/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class EstructurasControl {

    public static void main(String[] args) {
//        int x = Integer.parseInt(JOptionPane.showInputDialog("Digite un número"));
//        if (x % 2 == 0) {
//            System.out.println("Es par");
//        } else {
//            System.out.println("Es impar");
//        }
//
//        int y = Integer.parseInt(JOptionPane.showInputDialog("Digite un número"));
//        if (x < y) {
//            System.out.println(String.format("X=%s es menor que Y=%s", x, y));
//        } else if (x > y) {
//            System.out.println(String.format("X=%s es mayor que Y=%s", x, y));
//        } else {
//            System.out.println(String.format("X=%s es igual que Y=%s", x, y));
//        }
        Logica log = new Logica();

//        int pas = Integer.parseInt(JOptionPane.showInputDialog("Cant. Pasajeros"));
//        int ejes = Integer.parseInt(JOptionPane.showInputDialog("Cant. Ejes"));
//        double costo = Double.parseDouble(JOptionPane.showInputDialog("Costo Vehículo"));
//        
//        System.out.println("El costo total del vehículo es: $" + log.costoTotal(pas, ejes, costo));
//        int nota = Integer.parseInt(JOptionPane.showInputDialog("Nota"));
//        JOptionPane.showMessageDialog(null, log.asignarNota(nota));
        String menu = "1. Ejercicio #1\n"
                + "2. Ejercicio #2\n"
                + "3. Ejercicio #3\n"
                + "4. Salir";
        int opcion = 4;//Integer.parseInt(JOptionPane.showInputDialog(menu));
        switch (opcion) {
            case 1:
                System.out.println("Entró al 1");
                break;
            case 2:
                System.out.println("Entró al 2");
                break;
            case 3:
                System.out.println("Entró al 3");
                break;
            case 4:
                System.out.println("Saliendo del sistema!!");
                break;
            default:
                System.out.println("Opción inválda");
                break;
        }
        
        int dia = Integer.parseInt(JOptionPane.showInputDialog("Número de día"));
        System.out.println(log.obtenerDia(dia));
        
    }
}
