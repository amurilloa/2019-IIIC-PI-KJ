/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructurascontrol;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    /*
    nivel de acceso + tipo de retorno + nombre del método (tipo parametro + parametro ) { }
    private: solo lo puedo utilizar dentro de esta clase
    public: que lo puedo utilizar en cualquier parte del proyecto
    tipo de retorno: int, String, char, double, boolean, Perro ....
    nombre del método: algo significativo que represente lo que hace el método ej. sumar
    tipo de parámetro: int, String, char, double, boolean, Perro ....
    parametro: es el nombre de la variable que trae los valores al método 
     */
    /**
     * Suma dos números enteros
     *
     * @param n1 int primer número a sumar
     * @param n2 int segundo número a sumar
     * @return int con el resultado de sumar n1 y n2
     */
    public int sumar(int n1, int n2) {
        return n1 + n2;
    }

    /**
     * Resta dos números enteros, al primer número se le resta el valor del
     * segundo
     *
     * @param n1 int primer el número
     * @param n2 int segundo número
     * @return int resultado de restar a n1 el valor de n2
     */
    public int restar(int n1, int n2) {
        int res = n1 - n2;
        return res;
    }

    public static int multiplicar(int n1, int n2) {
        int res = n1 * n2;
        return res;
    }

    public double costoTotal(int pas, int ejes, double costo) {
        double impBase = costo * 0.01;
        double extra = 0;

        //Cantidad de pasajeros 
        if (pas < 20) {
            extra = impBase * 0.01;
        } else if (pas >= 20 && pas <= 60) {
            extra = impBase * 0.05;
        } else {
            extra = impBase * 0.08;
        }

        //Numero de ejes
        if (ejes == 2) {
            extra += impBase * 0.05;
        } else if (ejes == 3) {
            extra += impBase * 0.10;
        } else if (ejes > 3) {
            extra += impBase * 0.15;
        }

        double imp = impBase + extra;
        double costoTotal = costo + imp;
        return costoTotal;
    }

    /**
     * Convierte de una nota númerica, a una nota en letras
     *
     * @param nota en números
     * @return nota en letras
     */
    public String asignarNota(int nota) {
        if (nota < 70) {
            return "Insuficiente";
        } else if (nota < 80) {
            return "Bien";
        } else if (nota < 90) {
            return "Notable";
        } else {
            return "Sobresaliente";
        }
    }

    public String tipoMusico(int can, int par) {
        if (can >= 7 && can <= 10) {
            if (par == 0) {
                return "Músico Naciente";
            } else if (par >= 1 && par <= 5) {
                return "Músico Estelar";
            }
        } else if (can > 10 && par > 5) {
            return "Músico consagrado";
        }

        return "Músico en formación";

    }

    public String obtenerDia(int num) {
        switch (num) {
            case 1:
                return "Domingo";
            case 2:
                return "Lunes";
            case 3:
                return "Martes";
            case 4:
                return "Miércoles";
            case 5:
                return "Jueves";
            case 6:
                return "Viernes";
            case 7:
                return "Sábado";
            default:
                return "Día inválido";
        }
    }
}
