/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class Main {

    public static void main(String[] args) {
        Logica log = new Logica();
        String menu = "Práctica Extra\n"
                + "1. Salir\n"
                + "2. Tablas de Multiplicar\n"
                + "3. \n";

        String mensaje = menu;
        APP:
        while (true) {

            int op = Integer.parseInt(JOptionPane.showInputDialog(mensaje));

            mensaje = menu;
            switch (op) {
                case 1:
                    break APP;
                case 2:
                    int tabla = Integer.parseInt(JOptionPane.showInputDialog("Tabla de Multiplicar"));
                    JOptionPane.showMessageDialog(null, log.tablaMultiplicar(tabla));
                    break;
                default:
                    mensaje = ">> Opción Inválida\n\n" + menu;
            }
        }
    }
}
