/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica;

/**
 *
 * @author Allan Murillo
 */
public class Logica {

    public String tablaMultiplicar(int tabla) {
        String res = "";
        int cont = 0;
        while (cont < 10) {
            String linea = String.format("%dx%d=%d\n", tabla, cont, tabla * cont);
            res += linea;
            cont++;
        }
        return res;
    }
}
