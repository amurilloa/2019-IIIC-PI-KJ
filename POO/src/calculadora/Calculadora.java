/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

/**
 *
 * @author Allan Murillo
 */
public class Calculadora {

    public double resolver(int n1, int n2, char sim) {
        switch (sim) {
            case '+':
                return n1 + n2;
            case '-':
                return n1 - n2;
            case '*':
                return n1 * n2;
            case '/':
                return (double) n1 / n2;
            default:
                return 0;
        }
    }

    public int sumar(int n1, int n2) {
        return n1 + n2;
    }

    public int restar(int n1, int n2) {
        return n1 - n2;
    }

    public int multiplicar(int n1, int n2) {
        return n1 * n2;
    }

    public double dividir(int n1, int n2) {
        return (double) n1 / n2;
    }
}
