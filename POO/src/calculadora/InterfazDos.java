/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class InterfazDos {

    public static void main(String[] args) {
        Calculadora log = new Calculadora();
        String[] opciones = {"(+) Sumar", "(-) Restar", "(*) Multiplicar",
            "(/) Dividir"};
        String mensaje = "";
        CALC:
        while (true) {
            String op = (String) JOptionPane.showInputDialog(null, mensaje + "Seleccione una opción",
                    "Calculadora - UTN v0.1", JOptionPane.PLAIN_MESSAGE,
                    null, opciones, opciones[0]);
            op = op == null ? "(s) Salir" : op;
            mensaje = "";
            
            switch (op.charAt(1)) {
                case '+':
                    int n1 = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                    int n2 = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                    int res = log.sumar(n1, n2);
                    mensaje =  String.format(">> %d %s %d = %d\n", n1, op.charAt(1), n2, res);
                    break;
                case '-':
                    n1 = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                    n2 = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                    res = log.restar(n1, n2);
                    mensaje =  String.format(">> %d %s %d = %d\n", n1, op.charAt(1), n2, res);
                    break;
                case '*':
                    n1 = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                    n2 = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                    res = log.multiplicar(n1, n2);
                    mensaje =  String.format(">> %d %s %d = %d\n", n1, op.charAt(1), n2, res);
                    break;
                case '/':
                    n1 = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                    n2 = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                    double div = log.dividir(n1, n2);
                    mensaje =  String.format(">> %d %s %d = %.2f\n", n1, op.charAt(1), n2, div);
                    break;
                case 'S':
                case 's':
                    break CALC;
            }

        }
    }
}
