/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class Interfaz {

    public static void main(String[] args) {
        Calculadora calc = new Calculadora();
        String menu = "Calculadora - UTN v0.1\n"
                + "(+) Sumar\n"
                + "(-) Restar\n"
                + "(*) Multiplicar\n"
                + "(/) Dividir\n"
                + "(s) Salir\n"
                + "Seleccione una opción:";
        
        String x = "Rectangular";
        if("Rectangular".equals(x)){
        
        }
        
        while (true) {
            char sim = JOptionPane.showInputDialog(menu).charAt(0);
            if (sim == 's' || sim == 'S') {
                break;
            } else {
                int n1 = Integer.parseInt(JOptionPane.showInputDialog("#1"));
                int n2 = Integer.parseInt(JOptionPane.showInputDialog("#2"));
                double res = calc.resolver(n1, n2, sim);
                String txt = String.format("%d %s %d = %.2f", n1, sim, n2, res);
                JOptionPane.showMessageDialog(null, txt);
            }
        }
    }
}
