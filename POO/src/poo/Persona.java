/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author Allan Murillo
 */
public class Persona {

    private String nombre;
    private String apellido;
    private String telefono;
    private String genero;
    private String lugarTrabajo;
    private int salarioMensual;

    public Persona() {
    }

    public Persona(String nombre, String apellido, String telefono, String genero) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.genero = genero;
    }

    public Persona(String nombre, String apellido, String telefono, String genero, String lugarTrabajo, int salarioMensual) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.genero = genero;
        this.lugarTrabajo = lugarTrabajo;
        this.salarioMensual = salarioMensual;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getLugarTrabajo() {
        return lugarTrabajo;
    }

    public void setLugarTrabajo(String lugarTrabajo) {
        this.lugarTrabajo = lugarTrabajo;
    }

    public int getSalarioMensual() {
        return salarioMensual;
    }

    public void setSalarioMensual(int salarioMensual) {
        this.salarioMensual = salarioMensual;
    }

    @Override
    public String toString() {
        String txt = "Nombre: %s %s\n"
                + "Teléfono: %s\n"
                + "Género: %s";
        String temp = String.format("%s %d %.2f", "Resultado: ", 2, 2.3);
        String res = String.format(txt, nombre, apellido, telefono, genero);
        if (lugarTrabajo != null) {
            res += "\nLugar Trabajo: " + lugarTrabajo + "\nSalario Mensual: " + salarioMensual;
        }
        return res;
    }

}
