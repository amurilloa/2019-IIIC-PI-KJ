/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author Allan Murillo
 */
public class Garrobo {
    //Atributos 

    //nivel acceso(private) + tipo + nombre atributo
    private String nombre;
    private int distancia;
    private int tiempo;

    //Constructor >> Python: __init__
    public Garrobo() {

    }

    public Garrobo(String nombre) {
        this.nombre = nombre;
    }

    public Garrobo(String nombre, int distancia, int tiempo) {
        this.nombre = nombre;
        this.distancia = distancia;
        this.tiempo = tiempo;
    }

    //Métodos
    public double calcularDistancia(int t) {
        double d = t * velocidad();
        return d;
    }

    public double calcularTiempo(int d) {
        double t = d / velocidad();
        return t;
    }

    private double velocidad() {
        return (double) distancia / tiempo;
    }

    //Getters & Setters
    //GET: nivel acceso(public) + tipo del atributo + get+NombreAtributo() + { return atributo}
    public String getNombre() {
        return nombre;
    }

    //SET: nivel acceso(public) + void + set+NombreAtributo(tipo atributo parametro){atributo = parametro}
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    @Override
    public String toString() {
        return "Garrobo{" + "nombre=" + nombre + ", distancia=" + distancia
                + ", tiempo=" + tiempo
                + ", velocidad(m/s)=" + velocidad() + '}';
    }

}

//Nombre(singular), atributos, constructor, métodos, get, set, toString. 
//Entrevista: Class Persona: 
//Persona p1 = new Persona();
//p1.setNombre("lo que digito");

