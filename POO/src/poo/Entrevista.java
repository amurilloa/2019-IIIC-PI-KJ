/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

import javax.swing.JOptionPane;

/**
 *
 * @author Allan Murillo
 */
public class Entrevista {

    public static void main(String[] args) {
        
        String nom = JOptionPane.showInputDialog(null, "Nombre", "Entrevista", JOptionPane.QUESTION_MESSAGE);
        String ape = JOptionPane.showInputDialog(null, "Apellidos", "Entrevista", JOptionPane.QUESTION_MESSAGE);
        String tel = JOptionPane.showInputDialog(null, "Teléfono", "Entrevista", JOptionPane.QUESTION_MESSAGE);
        
        String[] generos = {"Femenino", "Masculino"};

        Object gen = JOptionPane.showInputDialog(null, "Género", "Entrevista",
                JOptionPane.QUESTION_MESSAGE, null, generos, generos[0]);

        Persona p1 = new Persona(nom, ape, tel, (String)gen);
        
        int tra = JOptionPane.showConfirmDialog(null, "¿Trabaja?",
                "Entrevista", JOptionPane.YES_NO_OPTION);

        if (tra == JOptionPane.YES_OPTION) {
            String lug = JOptionPane.showInputDialog(null, "Lugar de Trabajo", "Entrevista", JOptionPane.QUESTION_MESSAGE);
            String sal = JOptionPane.showInputDialog(null, "Salario Mensual", "Entrevista", JOptionPane.QUESTION_MESSAGE);
            p1.setLugarTrabajo(lug);
            p1.setSalarioMensual(Integer.parseInt(sal));   
        }
        System.out.println(p1);
        JOptionPane.showMessageDialog(null, p1);
    }

}
