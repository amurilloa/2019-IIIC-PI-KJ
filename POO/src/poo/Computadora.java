/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author Allan Murillo
 */
public class Computadora { //1-Nombre de la clase máyuscula y singular

    //2 - Atributos
    private String serie;
    private String marca;
    private String modelo;
    private String ram;
    private String procesador;
    private String so;
    private String discoDuro;
    private int precio;
    private boolean esTactil;

    //3 - Constructores: Son la forma de inicializar los objetos
    public Computadora() {
    }

    public Computadora(String serie, String marca,String modelo) {
        this.serie = serie;
        this.modelo = modelo;
        this.marca = marca;
    }

    public Computadora(String serie, String marca, String modelo, String ram,
            String procesador, String so, String discoDuro, int precio, boolean esTactil) {
        this.serie = serie;
        this.marca = marca;
        this.modelo = modelo;
        this.ram = ram;
        this.procesador = procesador;
        this.so = so;
        this.discoDuro = discoDuro;
        this.precio = precio;
        this.esTactil = esTactil;
    }

    //4 - Get/Set
    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getRam() {
        return ram;
    }

    public void setRam(String ram) {
        this.ram = ram;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public String getSo() {
        return so;
    }

    public void setSo(String so) {
        this.so = so;
    }

    public String getDiscoDuro() {
        return discoDuro;
    }

    public void setDiscoDuro(String discoDuro) {
        this.discoDuro = discoDuro;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public boolean isEsTactil() {
        return esTactil;
    }

    public void setEsTactil(boolean esTactil) {
        this.esTactil = esTactil;
    }

    //5 - toString
    @Override
    public String toString() {
        return "Computadora{" + "serie=" + serie + ", marca=" + marca + ", modelo=" + modelo + ", ram=" + ram + ", procesador=" + procesador + ", so=" + so + ", discoDuro=" + discoDuro + ", precio=" + precio + ", esTactil=" + esTactil + '}';
    }
    
}
