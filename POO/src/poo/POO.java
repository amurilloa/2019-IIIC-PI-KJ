/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poo;

/**
 *
 * @author Allan Murillo
 */
public class POO {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

//        Logica log = new Logica(); // Creamos un objeto o instancia llamada log de tipo Logica 
//
//        int s = log.sumar(12, 2); //La instancia nos permite acceder a los métodos o atributos públicos de la clase
//        System.out.println(s);
//
//        System.out.println(log.restar(21, 5));
//
//        int m = Logica.multiplicar(5, 4);
//        System.out.println(m);
//
//        Garrobo g1 = new Garrobo();
//        Garrobo g2 = new Garrobo("Juancito");
//        Garrobo g3 = new Garrobo("Lucy", 20, 9);
//        
//        g1.setNombre("Garrobito");
//        g1.setDistancia(10);
//        g1.setTiempo(5);
//
//        g2.setDistancia(10);
//        g2.setTiempo(5);
//
//        System.out.println("G1");
//        System.out.println(g1);
//        System.out.println("G2");
//        System.out.println(g2);
//        System.out.println("G3");
//        System.out.println(g3);
//      System.out.println(g1.calcularDistancia(50));
//      System.out.println(g1.calcularTiempo(15));
        Computadora c0 = new Computadora();
        Computadora c1 = new Computadora("PC-12345", "DELL", "G5");
        c0.setSerie("PC-00001");
        System.out.println(c0);
        System.out.println(c1);
        
        //Definir 2 clases min 6 atributos cada una, contructor, getters, setters, toString. 
        //Crear un objeto de cada una con los datos llenos 
        //Imprimir el estado del objeto 
    }

}
