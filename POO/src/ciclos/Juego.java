/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciclos;

/**
 *
 * @author Allan Murillo
 */
public class Juego {

    private int intentos;
    private final int aleatorio;

    private final int MAX_INT = 5;
    private final int MAX_ALE = 20;

    public Juego() {
        intentos = 1;
        aleatorio = (int) (Math.random() * MAX_ALE) + 1;
        //TODO: Eliminar la impresión de la clase. 
        System.out.println(aleatorio);
    }

    public boolean continuar() {
        return intentos <= MAX_INT;
    }

    public String adivinar(int num) {
        intentos++;
        if (num == aleatorio) {
            intentos = MAX_INT + 1;
            return "Ganaste adivinaste el número aleatorio";
        } else if (intentos > MAX_INT) {
            return "Perdiste acabaron tus intentos";
        } else if (num > aleatorio) {
            return "El número es menor";
        } else {
            return "El número es mayor";
        }
    }

}
