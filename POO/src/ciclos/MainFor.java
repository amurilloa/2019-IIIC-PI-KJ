/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciclos;

/**
 *
 * @author Allan Murillo
 */
public class MainFor {

    public static void main(String[] args) {
//        int con = 1;
//        while (con <= 10) {
//            System.out.println(con++);
//        }

        //1..10
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
        }

        //10..1
        for (int i = 10; i > 0; i--) {
            System.out.println(i);
        }

        
        String[] datos = {"Juan", "Maria", "Francisco", "Juana"};
        for (String x : datos) {
            System.out.println(x);
        }

    }
}
