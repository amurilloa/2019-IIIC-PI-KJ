/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciclos;

import pintor.Util;

/**
 *
 * @author Allan Murillo
 */
public class MainDoWhile {

    public static void main(String[] args) {
        String menu = "1. Ejercicio #1\n"
                + "2. Ejercicio #2\n"
                + "3. Ejercicio #3\n"
                + "4. Salir";
        
        
        int op;
        do{
            op = Util.leerInt(menu);
            if(op == 1){
                System.out.println("Ejercicio 1");
            }
        } while(op!=4);
        
    }
}
