/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciclos;

import pintor.Util;

/**
 *
 * @author Allan Murillo
 */
public class JuegaPrincipal {

    public static void main(String[] args) {
        Juego j = new Juego();
        while (j.continuar()) {
            int num = Util.leerInt("Digite un número entre 1-20");
            System.out.println(j.adivinar(num));
        }
    }
}
