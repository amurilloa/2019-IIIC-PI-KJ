/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ciclos;

import pintor.Util;

/**
 *
 * @author Allan Murillo
 */
public class JuegoMainV1 {

    public static void main(String[] args) {
        int alea = (int) (Math.random() * 20) + 1;
        System.out.println(alea);
        boolean gano = false;

        for (int i = 1; i <= 5; i++) {
            System.out.println("Intento #" + i);
            int num = Util.leerInt("Digite un número entre 1-20");
            if (num == alea) {
                gano = true;
                System.out.println("Ganaste adivinaste el número aleatorio");
                break;
            } else if (num > alea) {
                System.out.println("El número es menor");
            } else {
                System.out.println("El número es mayor");
            }
        }

        if (!gano) {
            System.out.println("Perdiste acabaron tus intentos");
        }

    }
}
