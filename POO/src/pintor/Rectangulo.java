/*
 * Crear una clase Rectangulo, la cual tiene dos atributos llamados, alto y ancho de tipo double. 
 * El contructor de la clase inicializa los atributos con los parámetros correspondientes. 
 * Debe tener un método para calcular el área del rectángulo.
 */
package pintor;

/**
 *
 * @author Allan Murillo
 */
public class Rectangulo {

    private double alto;
    private double ancho;

    public Rectangulo() {
    }

    public Rectangulo(double alto, double ancho) {
        this.alto = alto;
        this.ancho = ancho;
    }

    public double getArea() {
        return alto * ancho;
    }

    public double getAlto() {
        return alto;
    }

    public void setAlto(double alto) {
        this.alto = alto;
    }

    public double getAncho() {
        return ancho;
    }

    public void setAncho(double ancho) {
        this.ancho = ancho;
    }

    @Override
    public String toString() {
        return "Rectangulo{" + "alto=" + alto + ", ancho=" + ancho + '}';
    }

}
