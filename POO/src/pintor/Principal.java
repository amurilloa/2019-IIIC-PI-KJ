/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pintor;

/**
 *
 * @author Allan Murillo
 */
public class Principal {

    public static void main(String[] args) {

        String menuUno = "1. Nueva cotización\n"
                + "2. Salir";

        String[] tipos = {"Circular", "Rectangular"};

        APP:
        while (true) {
            int op = Util.leerInt(menuUno);
            if (op == 1) {
                Cotizacion c = new Cotizacion();
                c.setCliente(Util.leerTexto("Nombre del Cliente: "));

                //Paredes
                do {
                    double al = Util.leerNum("Alto(pared):");
                    double an = Util.leerNum("Ancho(pared):");
                    c.agregarPared(al, an);

                    //Ventanas
                    while (Util.confirmar("¿Desea agregar una ventana ?")) {
                        String tipo = Util.seleccionar("Tipo de Ventana", tipos);
                        if ("Rectangular".equals(tipo)) {
                            al = Util.leerNum("Alto(ventana):");
                            an = Util.leerNum("Ancho(ventana):");
                            c.agregarVentana(al, an);
                        } else {
                            double di = Util.leerNum("Diametro(ventana):");
                            c.agregarVentana(di);
                        }
                    }
                } while (Util.confirmar("¿Desea agregar otra pared?"));
                Util.mostrar(c.toString());

            } else {
                Util.mostrar("Gracias por utilizar la aplicación");
            }
            break;
        }

    }
}
