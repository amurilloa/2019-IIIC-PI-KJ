/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pintor;

/**
 *
 * @author Allan Murillo
 */
public class Cotizacion {

    private String cliente;
    private double totalAreaP;
    private double totalAreaV;

    private final int PRE_X_HORA = 30;
    private final int MIN_X_METRO = 10;
    private final int HRS_X_DIA_LAB = 10;

    public void agregarPared(double al, double an) {
        Rectangulo r = new Rectangulo(al, an);
        totalAreaP += r.getArea();
    }

    public void agregarVentana(double al, double an) {
        Rectangulo r = new Rectangulo(al, an);
        totalAreaV += r.getArea();
    }

    public void agregarVentana(double di) {
        Circunferencia c = new Circunferencia(di / 2);
        totalAreaV += c.getArea();
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public double getTotalAreaP() {
        return totalAreaP;
    }

    private String getTiempo(double area) {
        int min = (int) Math.round(area * MIN_X_METRO);//1988

        int h = min / 60;
        min = min % 60;
        int d = h / HRS_X_DIA_LAB;
        h = h % HRS_X_DIA_LAB;
        return String.format("%d días, %d hrs, %d min", d, h, min);
    }

    private int getHoras(double area) {
        int min = (int) Math.round(area * MIN_X_METRO);//1988
        int h = min / 60;
        return h;
    }

    @Override
    public String toString() {
        double areaPint = totalAreaP - totalAreaV;
        double costo = getHoras(areaPint) * PRE_X_HORA;
        String tiempo = getTiempo(areaPint);

        String txt = "Cliente: %s\n"
                + "Área Paredes: %.2f\n"
                + "Área Ventanas: %.2f\n"
                + "Área a Pintar: %.2f\n"
                + "Duración Aprox.: %s\n"
                + "Total: $%.2f";

        return String.format(txt, cliente, totalAreaP, totalAreaV, areaPint, tiempo, costo);
    }

}
