/*
 * Crear una clase Circunferencia, la cual tiene un atributo llamado radio de tipo double.
 * El contructor de la clase incializa el radio de la clase con el parámetro correspondiente. 
 * Debe tener un método para calcular el área  de la circunferencia. 
 */
package pintor;

/**
 *
 * @author Allan Murillo
 */
public class Circunferencia {

    private double radio;

    public Circunferencia() {
    }

    public Circunferencia(double radio) {
        this.radio = radio;
    }

    public double getArea() {
        return Math.PI * Math.pow(radio, 2);
    }

    public double getRadio() {
        return radio;
    }

    public void setRadio(double radio) {
        this.radio = radio;
    }

    @Override
    public String toString() {
        return "Circunferencia{" + "radio=" + radio + '}';
    }
    
}
