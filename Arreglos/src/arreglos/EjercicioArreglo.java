/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author Allan Murillo
 */
public class EjercicioArreglo {

    private final int[] arreglo;

    public EjercicioArreglo() {
        arreglo = new int[]{1, 3, 4, 6, 3, 6, 8, 2, 4};
    }

    public EjercicioArreglo(int[] arreglo) {
        this.arreglo = arreglo;
    }

    public double promedio() {
        double suma = 0;
        for (int valor : arreglo) {
            suma += valor;
        }
        double promedio = suma / arreglo.length;
        return promedio;
    }

    public int buscarElemento(int elemento) {
        for (int i = 0; i < arreglo.length; i++) {
            int valor = arreglo[i];
            if (valor == elemento) {
                return i;
            }
        }
        return -1;
    }

    public String imprimir() {
        String txt = "";
        for (int i : arreglo) {
            txt += i + " ";
        }
        return txt;
    }
}
