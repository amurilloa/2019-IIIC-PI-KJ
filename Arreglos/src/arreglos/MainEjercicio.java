/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author Allan Murillo
 */
public class MainEjercicio {

    public static void main(String[] args) {
        EjercicioArreglo log = new EjercicioArreglo();
        System.out.println(log.imprimir());
        System.out.println(log.promedio());
        System.out.println(log.buscarElemento(4));
        int[] arr = {1,2,3,4};
        EjercicioArreglo log2 = new EjercicioArreglo(arr);
        System.out.println(log2.imprimir());
        System.out.println(log2.promedio());
        System.out.println(log2.buscarElemento(5));
    }
}
