/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author Allan Murillo
 */
public class LogicaArregloParametros {

    //Simula un paso de de parametro por valor (realmente solo aplica tipos de datos
    //primitivos)
    public int[] convertirPares(int[] param) {
        int[] res = new int[param.length];

        for (int i = 0; i < param.length; i++) {
            if (param[i] % 2 != 0) {
                res[i] = param[i] + 1;
            } else {
                res[i] = param[i];
            }
        }
        return res;
    }

    //Paso de parámetro por referencia, si modifican la variable automáticamente se 
    //modifica en el sitio original(no funciona con tipos de datos primitivos)
    public void convertirImpar(int[] param) {
        for (int i = 0; i < param.length; i++) {
            if (param[i] % 2 == 0) {
                param[i] += 1;
            }
        }
    }

}
