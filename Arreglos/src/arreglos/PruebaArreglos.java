/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author Allan Murillo
 */
public class PruebaArreglos {

    public static void main(String[] args) {

        String s1 = "Hola";
        String s2 = "Hola";
        System.out.println(s1.equals(s2));
        s1 = "hola";
        System.out.println(s1.equalsIgnoreCase(s2));
        
        Perro p1 = new Perro(1, "CAPITÁN", "Pastor");
        Perro p2 = new Perro(1, "capitán", "Pastor");
        
        System.out.println(p1.equals(p2));
        
        int cant = Util.leerInt("Cantidad:");
        
        int n01 = 0;
        int n02 = 0;
        int n03 = 0;
        int n04 = 0;
        int n05 = 0;
        int n06 = 0;
        int n07 = 0;
        int n08 = 0;
        int n09 = 0;
        int n10 = 0;

        int[] n = new int[cant];

        String menu = "1. Llenar \n"
                + "2. Imprimir\n"
                + "3. Salir";

        APP:
        do {
            int op = Util.leerInt(menu);
            switch (op) {
                case 1:
                    int temp = Util.leerInt("Número");
                    if (n01 == 0) {
                        n01 = temp;
                    } else if (n02 == 0) {
                        n02 = temp;
                    } else if (n03 == 0) {
                        n03 = temp;
                    } else if (n04 == 0) {
                        n04 = temp;
                    } else if (n05 == 0) {
                        n05 = temp;
                    } else if (n06 == 0) {
                        n06 = temp;
                    } else if (n07 == 0) {
                        n07 = temp;
                    } else if (n08 == 0) {
                        n08 = temp;
                    } else if (n09 == 0) {
                        n09 = temp;
                    } else if (n10 == 0) {
                        n10 = temp;
                    }

                    for (int i = 0; i < n.length; i++) {
                        if (n[i] == 0) {
                            n[i] = temp;
                            break;
                        }
                    }
                    break;
                case 2:
                    String txt = String.format("%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d\n%d",
                            n01, n02, n03, n04, n05, n06, n07, n08, n09, n10);
                    Util.mostrar(txt);

                    txt = "";
                    for (int x : n) {
                        txt += x + "\n";
                    }
                    Util.mostrar(txt);

                    break;
                case 3:
                    break APP;
            }

        } while (true);

    }
}
