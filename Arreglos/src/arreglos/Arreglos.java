/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

import java.util.Scanner;

/**
 *
 * @author Allan Murillo
 */
public class Arreglos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int numero = 12;
        int[] numeros1 = {32, 12, 3, 1, 53, 2};
        String[] palabras1 = {null, null, null, null, null};

        int[] numeros2 = new int[6];
        String[] palabras2 = new String[5];

        Perro[] perrera = new Perro[3];

        for (int i = 0; i < numeros1.length; i++) {
            System.out.print(numeros1[i] + " ");
        }

        numeros1[3] = -21;
        System.out.println("");

        for (int x : numeros1) {
            System.out.print(x + " ");
        }

        int[] nums = new int[5];
        for (int i = 0; i < nums.length; i++) {
            nums[i] = Util.leerInt("#" + (i + 1));
        }

        for (int n : nums) {
            System.out.println(n);
        }
        System.out.println("");
        Scanner sc = new Scanner(System.in);
        System.out.print("Digite un número: ");
        int x = sc.nextInt();
        //Llenarlo con números digitados por el usuario
        //Imprimir los números que están en el arreglo

//        perrera[0] = new Perro(21546, "Capitán", "Pastor Alemán");
//        Perro p = new Perro(21546, "Capitán", "Pastor Alemán");
//        perrera[1] = p;

        for (int i = 0; i < perrera.length; i++) {
            int ced = Util.leerInt("Cédula #"+(i)+":");
            String nom = Util.leerTexto("Nombre:");
            String raz = Util.leerTexto("Raza:");
            perrera[i] = new Perro(ced, nom, raz);
        }

        for (Perro perro : perrera) {
            System.out.println(perro);
        }

    }

}
