/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

import java.util.Objects;

/**
 *
 * @author Allan Murillo
 */
public class Perro {

    private int cedula;
    private String nombre;
    private String raza;

    public Perro() {
    }

    public Perro(int cedula, String nombre, String raza) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.raza = raza;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }


    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Perro other = (Perro) obj;
        if (this.cedula != other.cedula) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.raza, other.raza)) {
            return false;
        }
        return true;
    }

    
    
    @Override
    public String toString() {
        return "Perro{" + "cedula=" + cedula + ", nombre=" + nombre + ", raza=" + raza + '}';
    }
}
