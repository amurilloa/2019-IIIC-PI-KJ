/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arreglos;

/**
 *
 * @author Allan Murillo
 */
public class ArreglosParametro {
    public static void main(String[] args) {
        
        LogicaArregloParametros log = new LogicaArregloParametros();
                
        int[] arreglo = {1,2,3};
        
        for (int i : arreglo) {
            System.out.print(i + " ");
        }
         
        arreglo = log.convertirPares(arreglo);
        System.out.println("");
        
        for (int i : arreglo) {
            System.out.print(i + " ");
        }
        
       
        log.convertirImpar(arreglo);
       
         System.out.println("");
        
        for (int i : arreglo) {
            System.out.print(i + " ");
        }
        
        System.out.println("");
        
    }
}
